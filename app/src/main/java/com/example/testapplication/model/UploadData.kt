package com.example.testapplication.model

import java.io.File

data class UploadData(
    var typeId: Int,
    var name: String,
    var photo: File?)