package com.example.testapplication.model

data class PhotoTypeDtoOut(val id: Int, val name: String, val image: String)