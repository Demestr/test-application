package com.example.testapplication.model

import android.util.Log
import androidx.paging.PageKeyedDataSource
import com.example.testapplication.network.ApiFactory
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class ResponseDataSource(private val scope: CoroutineScope) :
    PageKeyedDataSource<Int, PhotoTypeDtoOut>() {

    private val api = ApiFactory.createApi()

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, PhotoTypeDtoOut>
    ) {
        scope.launch {
            try {
                val response = api.getPageAsync(0)
                val pagePhotoTypeDtoOut = response.await()
                val listPhotoTypeDtoOut = pagePhotoTypeDtoOut?.content
                callback.onResult(
                    listPhotoTypeDtoOut.orEmpty(),
                    null,
                    1
                )
            } catch (exception: Exception) {
                Log.e("PostsDataSource", "Failed to fetch data!")
            }

        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, PhotoTypeDtoOut>) {
        scope.launch {
            try {
                val response = api.getPageAsync(page = params.key)
                val pagePhotoTypeDtoOut = response.await()
                val listPhotoTypeDtoOut = pagePhotoTypeDtoOut?.content
                callback.onResult(
                    listPhotoTypeDtoOut.orEmpty(),
                    params.key.inc()
                )
            } catch (exception: Exception) {
                Log.e("PostsDataSource", "Failed to fetch data!")
            }

        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, PhotoTypeDtoOut>) {
        scope.launch {
            try {
                val response = api.getPageAsync(page = params.key)
                val pagePhotoTypeDtoOut = response.await()
                val listPhotoTypeDtoOut = pagePhotoTypeDtoOut?.content
                callback.onResult(
                    listPhotoTypeDtoOut.orEmpty(),
                    params.key.dec()
                )
            } catch (exception: Exception) {
                Log.e("PostsDataSource", "Failed to fetch data!")
            }

        }
    }
}