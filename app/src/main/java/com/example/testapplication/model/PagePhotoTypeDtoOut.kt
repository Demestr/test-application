package com.example.testapplication.model

data class PagePhotoTypeDtoOut(
    val page: Int,
    val pageSize: Int,
    val totalElements: Int,
    val totalPages: Int,
    val content: List<PhotoTypeDtoOut>
)