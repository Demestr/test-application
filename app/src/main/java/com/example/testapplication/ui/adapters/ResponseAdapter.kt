package com.example.testapplication.ui.adapters

import android.graphics.Color
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.testapplication.R
import com.example.testapplication.databinding.ItemListBinding
import com.example.testapplication.model.PhotoTypeDtoOut

class ResponseAdapter(private val onCardClickListener: CardClickListener) : PagedListAdapter<PhotoTypeDtoOut, ResponseAdapter.ResponseHolder>(
    DIFF_CALLBACK
){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ResponseHolder {
        val binding = ItemListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ResponseHolder(binding)
    }

    override fun onBindViewHolder(holder: ResponseHolder, position: Int) {
        val response = getItem(position)
        holder.bind(response)
    }

    companion object{
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<PhotoTypeDtoOut>() {
            // The ID property identifies when items are the same.
            override fun areItemsTheSame(oldItem: PhotoTypeDtoOut, newItem: PhotoTypeDtoOut) =
                oldItem.id == newItem.id

            // If you use the "==" operator, make sure that the object implements
            // .equals(). Alternatively, write custom data comparison logic here.
            override fun areContentsTheSame(oldItem: PhotoTypeDtoOut, newItem: PhotoTypeDtoOut) = oldItem == newItem
        }
    }

    inner class ResponseHolder(private val binding: ItemListBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(photoTypeDtoOut: PhotoTypeDtoOut?) {
            binding.textNameItem.text = photoTypeDtoOut?.name
            val spannable = SpannableString("id: " + photoTypeDtoOut?.id.toString())
            spannable.setSpan(ForegroundColorSpan(Color.BLUE), 4, spannable.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            binding.textIdItem.text = spannable
            binding.cardItem.setOnClickListener {
                onCardClickListener.onCardClick(photoTypeDtoOut)
            }
            Glide.with(binding.root)
                .load(photoTypeDtoOut?.image)
                .placeholder(R.drawable.profile_placeholder)
                .circleCrop()
                .dontAnimate()
                .into(binding.imageItem)
        }

    }

    interface CardClickListener{
        fun onCardClick(photoTypeDtoOut: PhotoTypeDtoOut?)
    }
}