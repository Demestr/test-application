package com.example.testapplication.ui.fragments.main

import android.accounts.NetworkErrorException
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.example.testapplication.model.PhotoTypeDtoOut
import com.example.testapplication.model.ResponseDataSource
import com.example.testapplication.model.UploadData
import com.example.testapplication.network.ApiFactory
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.net.SocketTimeoutException

class MainViewModel : ViewModel() {

    private val uploadData = UploadData(-1, "", null)
    val pagedLiveData: LiveData<PagedList<PhotoTypeDtoOut>>

    init {
        val config = PagedList.Config.Builder()
            .setPageSize(20)
            .setEnablePlaceholders(false)
            .build()
        pagedLiveData = initializedPagedListBuilder(config).build()
    }

    private fun initializedPagedListBuilder(config: PagedList.Config):
            LivePagedListBuilder<Int, PhotoTypeDtoOut> {

        val dataSourceFactory = object : DataSource.Factory<Int, PhotoTypeDtoOut>() {
            override fun create(): DataSource<Int, PhotoTypeDtoOut> {
                return ResponseDataSource(viewModelScope)
            }
        }
        return LivePagedListBuilder<Int, PhotoTypeDtoOut>(dataSourceFactory, config)
    }

    fun setId(id: Int) {
        uploadData.typeId = id
    }

    fun setName(name: String){
        uploadData.name = name
    }

    fun setBitmap(path: String) {
        val file = File(path)
        uploadData.photo = file
        postPhoto(file)
    }

    private fun postPhoto(file: File) {
        try {
            val fileReqBody = RequestBody.create(MediaType.parse("image/*"), file)
            val requestFile = MultipartBody.Part.createFormData("photo", file.name, fileReqBody)
            val response =
                ApiFactory.createApi().uploadPhotoAsync(uploadData.typeId, uploadData.name, requestFile)
            response.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    Log.e("errorTest", "fail")
                }

                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {
                    Log.e("errorTest", "success")
                }

            })
        } catch (ex: NetworkErrorException) {
            Log.e("errorTest", ex.localizedMessage ?: "")
        } catch (socketEx: SocketTimeoutException) {
            Log.e("errorTest", socketEx.localizedMessage ?: "")
        }
    }
}