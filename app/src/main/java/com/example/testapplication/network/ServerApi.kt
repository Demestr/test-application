package com.example.testapplication.network

import com.example.testapplication.model.PagePhotoTypeDtoOut
import kotlinx.coroutines.Deferred
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface ServerApi {

    @GET("photo/type")
    fun getPageAsync(@Query("page") page: Int): Deferred<PagePhotoTypeDtoOut?>

    @Multipart
    @POST("photo")
    fun uploadPhotoAsync(
        @Part("typeId")
        id: Int,
        @Part("name")
        name: String,
        @Part
        image: MultipartBody.Part): Call<ResponseBody>
}